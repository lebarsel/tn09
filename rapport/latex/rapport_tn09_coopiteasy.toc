\contentsline {part}{I\hspace {1em}Pr\IeC {\'e}sentation de Coop IT Easy et de son activit\IeC {\'e}}{4}{part.1}
\contentsline {section}{\numberline {1}La coop\IeC {\'e}rative}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Historique}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Statut l\IeC {\'e}gal}{5}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Finalit\IeC {\'e}s}{5}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Le logiciel Odoo, c\IeC {\oe }ur d'activit\IeC {\'e}}{6}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Participation au sein du milieu social}{6}{subsection.1.5}
\contentsline {section}{\numberline {2}Organisation interne}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Postes}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Gouvernance}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Gestion de projet}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Outils de travail}{8}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Processus de d\IeC {\'e}veloppement}{8}{subsection.2.5}
\contentsline {part}{II\hspace {1em}Mission}{9}{part.2}
\contentsline {section}{\numberline {3}Le client : un supermarch\IeC {\'e} coop\IeC {\'e}ratif, la \textit {BEES Coop}}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Diff\IeC {\'e}rents r\IeC {\'e}gimes de travail}{10}{subsection.3.1}
\contentsline {section}{\numberline {4}Sujet initial}{11}{section.4}
\contentsline {section}{\numberline {5}Evolution du sujet}{11}{section.5}
\contentsline {section}{\numberline {6}Planning}{11}{section.6}
\contentsline {section}{\numberline {7}M\IeC {\'e}thodes de travail}{12}{section.7}
\contentsline {section}{\numberline {8}Langages, outils et proc\IeC {\'e}dures}{13}{section.8}
\contentsline {subsection}{\numberline {8.1}Langages de programmation}{13}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Outils}{13}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Proc\IeC {\'e}dures}{15}{subsection.8.3}
\contentsline {section}{\numberline {9}Avant le projet : \IeC {\'e}tat des lieux}{15}{section.9}
\contentsline {subsection}{\numberline {9.1}Un module de shifts pr\IeC {\'e}-existant}{15}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Gestion des pr\IeC {\'e}sences sur papier}{17}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Cartes de membre}{18}{subsection.9.3}
\contentsline {part}{III\hspace {1em}R\IeC {\'e}alisations}{19}{part.3}
\contentsline {section}{\numberline {10}Consultation du code}{19}{section.10}
\contentsline {subsection}{\numberline {10.1}D\IeC {\'e}p\IeC {\^o}ts GitHub}{19}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Structure du module}{19}{subsection.10.2}
\contentsline {section}{\numberline {11}Conception}{20}{section.11}
\contentsline {subsection}{\numberline {11.1}Cas d'utilisation}{20}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Solution propos\IeC {\'e}e}{21}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Int\IeC {\'e}gration dans le module de gestion des shifts}{21}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Design fonctionnel}{22}{subsection.11.4}
\contentsline {subsection}{\numberline {11.5}Architecture}{24}{subsection.11.5}
\contentsline {subsection}{\numberline {11.6}Simplification du statut des shifts}{26}{subsection.11.6}
\contentsline {section}{\numberline {12}Descriptif technique}{27}{section.12}
\contentsline {subsection}{\numberline {12.1}G\IeC {\'e}n\IeC {\'e}ration automatique}{27}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Remplissage}{28}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Validation}{30}{subsection.12.3}
\contentsline {subsection}{\numberline {12.4}Administration}{33}{subsection.12.4}
\contentsline {subsection}{\numberline {12.5}Groupes de s\IeC {\'e}curit\IeC {\'e} et droits d'acc\IeC {\`e}s}{39}{subsection.12.5}
\contentsline {section}{\numberline {13}Finalisation}{40}{section.13}
\contentsline {subsection}{\numberline {13.1}Traductions}{40}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Documentation}{41}{subsection.13.2}
\contentsline {section}{\numberline {14}Portage du module de shifts en version 12.0}{42}{section.14}
\contentsline {subsection}{\numberline {14.1}Portage du code}{42}{subsection.14.1}
\contentsline {subsection}{\numberline {14.2}Migration des donn\IeC {\'e}es}{44}{subsection.14.2}
\contentsline {section}{\numberline {15}Mise en perspective}{44}{section.15}
\contentsline {subsection}{\numberline {15.1}Contributions}{45}{subsection.15.1}
\contentsline {subsection}{\numberline {15.2}Complexit\IeC {\'e} du syst\IeC {\`e}me}{45}{subsection.15.2}
\contentsline {subsection}{\numberline {15.3}Flexibilit\IeC {\'e} du syst\IeC {\`e}me}{45}{subsection.15.3}
\contentsline {part}{Conclusion}{46}{part*.22}
\contentsline {chapter}{R\'ef\'erences}{47}{subparagraph.15.3.0.0.1}
