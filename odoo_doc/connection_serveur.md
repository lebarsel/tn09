# Connection au serveur test

`ssh root@gestion-test.bees-coop.be`
`su - odoo`

La gestion du serveur se fait avec le service *ociedoo* :
`sudo systemctl status odoo`

- Mettre à jour le code avec git-aggregator
- Stopper odoo, mettre à jour et le redémarrer
`ociedoo update-module bdd_name module_name`


Pour voir les db : `ociedoo list-db`
