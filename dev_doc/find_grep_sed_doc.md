## Find, Grep and Sed documentation

### Find

`find . -type f -name "*bu"`

`find . -type f -name "*bu" -delete`

Execute an arbitrary command (`{}` will be expanded to the found file’s name)

`find . -type f -name "*backup" -exec rm -f {} \;`

In a remote location

`find /home/curtis -type f -name "*.log" -execdir gzip {} \;`

### Grep

### SED

Two ways of invoking SED :
`sed [-n] [-e] 'command(s)' files `
`sed [-n] -f scriptfile files`

Standard options
`-n`: Default printing of pattern buffer.  
`-e` : Next argument is an editing command.
