# Attendance Sheets - Tests unitaires

Ce document présente la création de tests unitaires pour les feuilles de présence du module `beesdoo_shift`.

### A prendre en compte

Tests avec 2 utilisateurs différents !

## A tester
Si l'utilisateur scanne et qu'il appartient au groupe _CooperativeAdmin_, la validation doit être effective

### Default Task Type

Vérifier qu'il est bien nul et que cela ne pose pas de souci avant de lui donner une valeur.
Changer le défault task type
Voir que cela se répercute sur la méthode `default_task_type_id` d'`AttendanceSheetShift`
S'il n'y a pas de défault task type, tester ce qu'il se passe

### Attendance Sheet creation (administrator) OK

#### SetUp

**Créer des shifts dans un créneau horaire avec :**

* Avec deux TaskType différents
* Créer un shift vide
* Créer un shift regular worker
* Créer un shift regular worker avec remplaçant
* Créer un shift volant

Vérifier le nombre de travailleurs
Vérifier que les expected workers du Task Template sont bien ajoutés

**Créer des shifts dans pour un autre horaire, toujours dans l'intervalle de 20 minutes**

**Créer des shifts dans pour un horaire avant la feuille de présence**

**Créer des shifts dans pour un horaire après la feuille de présence**

#### Tests

* Vérifier que deux feuilles de présences sont crées, pour les deux créneaux différents inclus
* Vérifier que les shifts y sont bien ajoutés, sauf ceux vides
* Vérifier la cohérence entre les expected shifts et les Task réels
* Vérifier que les deux autres feuilles ne sont pas créées
* Vérifier que le nom n'est pas nul
* Vérifier que le time_slot n'est pas nul
* Vérifier que l'accord entre le `day` et le `start_time`
* Vérifier que le super coopérateur par défaut est juste
Vérifier que l'annotation est "" et que le champ booléen est bien lié


### Edition de la feuille (administrator & generic)

Tester la contrainte d'unicité. **Ces tests doivent-ils être inclus dans les tests précédents ?**

- Scan d'un travailleur expected : shift mis à jour
- Scan d'un travailleur expected (mais remplacant) : shift mis à jour
- Scan d'un travailleur non-expected régulier : shift mis à jour et valeurs par défaut
- Scan d'un travailleur non-expected volant : shift mis à jour et valeurs par défaut
- Scan d'un travailleur n'existant pas
- Scan sur des travailleurs déjà ajoutés
- Vérification de la cohérence des données ajoutées à la feuille de présence
- Ajout à la main de travailleurs déjà ajoutés

### Validation

#### Tests

Si admin :
- Une feuille déjà validée ne peut pas être validée
- Si le créneau n'a pas commencé, la feuille ne peut pas être validée
- Avancer l'heure, retester ça
- Les shifts doivent tous êtres remplis

- Donner des valeurs absent / done ainsi que 0, 1 et 2 compensations.
- Vérifier que les Expected shifts sont bien mis à jour
- Vérifier que les Added shifts sont mis à jours ou créés (shift vide rempli puis shifts créés)
- Vérifier qu'un email est bien envoyé à chaque fois

Si générique :
- Appel de `validate_with_checks` créé un wizard
- Tester les champs du wizard affichés en fonction du paramètre de login ou non
- L'annotation déjà remplie devrait se retrouver
- Remplissage des champs importants / fermeture du wizard et réouverture
- Tester validation par code barre et par login
- La fiche ne peut être validée que par un utilisateur super-coop ou avec groupe Attendance Sheet Admin

- Message d'info automatique dans l'annotation
- Champ annotated mis à jour

Tester les messages d'erreur après validation
Vérifier que le shift vide est bien mis à jour, avec un travailleur régulier (`is_regular` / `is_compensation` doivent être mis à jour également)


### Execution des CRONS (envoi d'emails)

### Inscription bloquée d'un volant 20 minutes avant shift (beesdoo website shift)
