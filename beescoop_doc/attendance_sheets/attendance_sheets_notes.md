# Attendance Sheets - Notes Diverses

# Situation test à la BEES 31/01/20

### Problèmes d'affichage

OK - Vue incomplète, retourne en haut lors du scan
OK - Enlever les champs "Max worker" et la phrase indicative
OK - Que se passe-t-il lors du scan dans un champ texte ?

### Traduction

"Et un" en trop lors de la validation

### Pour doc

Si on veut faire plusieurs shifts d'affilée, il faut scanner à chaque début de shift
Sauver régulièrement

### Pour test

S'il y a un remplçacant prévu, le scan est-il fonctionnel ?




# Liste d'ajout pour le module de shifts :

- pouvoir dire qu'un coopérateur est foireux (car il a annulé la veille deux fois) mais ça ne peut pas être vu par tt le monde... par qui... ? analyse à confirmer

Ajouter
- Shift Attendance => vue sur base du supercoopérateur qui est loggé pour ne voir que SES shifts

    • Vue Shift => vue actuelle est “Today” et groupé par type en vue kanban. La demande des gens qui utilisent le module au quotidien est de faire :
        ◦ Vue àpd aujourd’hui et dans le futur

Quand j’ai créé un nouveau supercoopérateur depuis le template de shift, je n’ai pas pu le retrouver comme supercoopérateur pour l’assigner à un autre template.

### A faire

- Validation date ?


  - Possible aussi de supprimer "Contacts" pour les supercoops ?
  -  Enrichir le groupe générique afin qu'il intègre le back-end, sans les contacts
  -   Mettre à jour le groupe Shift Attendance.

  -   Tous les autres statuts doivent se retrouver sur la feuille de présence,
     si le shift a déjà été pré-rempli : ou alors, **ils sont tous absents** (mais faut-il générer la feuille alors ?)
-   Intégrer fichiers .PO et changer la valeur des traductions. Intégrer Néerlandais


### Problèmes



Lors de la validation (2019-12-23 07:00 - 09:45) :

```
File "/home/odoo/custom/obeesdoo/beesdoo_shift/models/task.py", line 192, in   File "/home/odoo/custom/obeesdoo/beesdoo_shift/models/task.py", line 157, in write
    rec._update_state(vals['state'])
  File "/home/odoo/custom/obeesdoo/beesdoo_shift/models/task.py", line 192, in _update_state
    status = self.worker_id.cooperative_status_ids[0]
  File "/home/odoo/odoo/openerp/models.py", line 5778, in __getitem__
    return self._browse(self.env, (self._ids[key],))
IndexError: tuple index out of range
```

-   Mettre au clair les groupes
-   Documentation

**Done (check for tests)**

- Comment se déroule la validation des feuilles de présence sans système de carte, pour les autres supermarchés ? **Booléen de configuration activant le scan des cartes** ?
-   Mettre au clair les groupes d'accès et la documentation liée
-   Ecrire documentation et envoyer email
-   Mettre à jour les tests en accord avec les nouvelles modifications et terminer de les coder
-   par défaut le paramètre permettant d'utiliser un login n'est pas fonctionnel
-   Problème du worker_id d'un actual_task à False : la gestion du statut fait passer un travailleur à Unsuscribed et il est désinscrit du shift.
Appel de la méthode `unsubscribe_from_today` **qui le désinscrit à partir de minuit** et le retire donc du shift qu'il vient d'effectuer.
-   Régler problème de CRON envoi d'emails
-   Envoyer un email le dimanche soir à destination de tous les travailleurs (volants et réguliers) inscrits pour la semaine
-   Messages d'erreurs si gelé
-   Simplifier les statuts
-   Ajouter le numéro de la semaine sur la vue liste et sur le nom.
-   Changement pour un booléen
-   Ajouter configuration intervalle de génération des shifts & **affiner l'heure de génération** :
**Problème** : on ne pourra jamais générer les feuilles de présence un temps précis avant le début d'un shift car celui-ci est variable.
Il faudrait donc que le CRON tourne plus souvent que l'intervalle de génération !
-   Enlever la vue _Shift attendance_
-   L'annotation du wizard de validation doit être pré-remplie avec le message signifiant qu'un travailleur régulier effectue son shift régulier en _Added_. Cela signifie que si la feuille est remplie par un _CooperativeAdmin_, le message d'erreur ne sera pas enregistré.
-   Si l'utilisateur scanne et qu'il appartient au groupe _CooperativeAdmin_, la validation doit être effective
-   Enlever l'attribut _Cancelled_ du statut des shifts proposé dans la feuille de présence.
-   Enlever le default super_coop
-   Le _Maximum number of worker_ doit se trouver en haut et être calculé à partir des shifts créés et non pas des créneaux
-   Ne pas ajouter un shift annulé lors de la génération de la feuille de présence.

**Done**
-   Les shifts de compensation peuvent être enregistrés au préalable ! Pas un souci car on n'édite pas cette case pour les _Expected shifts_
-   Demo datas pour cartes
-   Envoi d'emails
-   Nettoyer code, faire correspondre aux guidelines
-   Envoyer Excel avec les termes à traduires
-   Enlever l'utilisateur générique du groupe _Employee_ mais garder le menu attendance sheet. Il faut lui donner uniquement accès au menu `dashboard`.
    Impossible, _Employee_ est la racine. A la limite, enlever la vue sur `Contact` à ce groupe pour la BEES.
-   Reprendre demo datas à jour de la 12
-   Changer nom pour quelque chose de plus lisible
-   Problème de la date et du nom, des recherches.
-   Ajouter filtres ("annotée", "date", "archivée", etc) pour la liste de toutes les feuilles
-   Afficher les super-coopérateurs
-   CRON de création
-   Possibilité de traduire facilement le message ajouté à l'annotation hardcodé en anglais et les autres messages (voir [doc](https://www.odoo.com/documentation/11.0/reference/guidelines.html#use-translation-method-correctly))
-   Cacher "Valider" si la fiche est validée (trop complexe pour "Modifier")
-   Reprendre les vues
-   Scan carte
    -   Pour la validation
    -   Pour l'ajout des travailleurs (essayer un wizard)
-   Problèmes d'affichage / compensations
-   Clarifier les droits
-   Ajouter champ de configuration, permettant de choisir le type de shift par défaut
-   Mark as Read : smart button
-   Ajouter l'archivage des feuilles de présence.
-   Une feuille de présence validée ne peut plus être modifiée par les administrateurs non plus, excepté en ce qui concerne les messages / internal notes.
-   Lever exception si remplacant est la même personne
-   Date du nom en ISO
-   Enlever même personne dans les travailleurs proposés en remplacement **semble être bon, à vérifier**
-   Ajouter champ code-barre lors de la validation
-   Désactiver la possibilité de cliquer sur un travailleur, un task type (affichés en bleu) dans la liste des shifts sur la feuille.
-   Ajout des shifts à la feuille de présence uniquement si worker set. Lors de la validation, check si shift n'existe pas (sans worker)
-   Enlever possibilité de créer un worker qui n'existe pas en remplacement (checker si possible pour generic)
-   Mettre toutes les vues de configuration dans le même fichier xml.

## Présentation à Rémy - bugs

-   Compensation field reste vide pour expected worker dans l'interface si on change de cette façon : **Absent**, **Present**, clic ailleurs.
  - Bug Odoo côté JS.
  - Champ compensation ne se met pas à jour lorsqu'on passe à **Cancelled**
-   Compensation affichée et enregistrée même si présent, même type de bug ! (voir `@api.onchange`) --> **Mettre des contraintes afin de tout de même prévenir ce type de bug**
-   Ajout d'un remplacant puis travailleur en added shift ne lève pas d'exception : **résolu**
-   Validation d'un shift expected met le champ worker à False, **sûrement car il était unsuscribed, à reproduire**
-   Bug édition d'un shift en test
    **Semble être normal le shift modifié n'avait sûrement pas de travailleur mais la fiche a tout de même été créée**

## A implémenter dans un second temps

-   Lorsqu'on modifie la feuille alors que validée, message d'erreur, mais message aussi si on essaie de quitter sans sauvegarder, ce qui n'a pas de sens !
-   Ajouter des messages d'avertissement et de réussite à propos de la validation des fiches.

## A statuer

-   Actuellement pas de contrainte lors de la création d'un shift concernant son unicité, certains sont
même dupliqués exprès. Or il y a une contrainte après coup, sur la feuille de présence, ce qui pourrait être source d'incohérences ou/et de bugs.
-   Nombre de travailleurs **Expected** et **Added** est superflu, il est déjà inscrit sur le form
-   Un travailleur exempté peut-il quand même faire un shift ?
-   Faut-il changer `regular` par `normal` pour le TaskMode ? Si oui, le faire partout.

-   Faut-il rajouter l'utilisateur générique ainsi que ses droits dans les datas ?
-   Faut-il afficher le nom des supercoopérateurs dans l'interface, en sachant qu'il y en a un pour chaque TaskType ?
-   Normalement un Expected Shift pour un travailleur régulier n'a pas à être mis en Regular (à vérifier avec la BEES), on ne fait donc la vérification que sur les Added Shift.
-   Comment afficher la date ? Actuellement, le nom figé de la fiche pose problème pour le tri. Cependant on ne peut afficher uniquement l'heure des feuilles de présence, à moins de créer deux champs computés.
-   Lorsque le `worker_id` n'est pas rempli pour un shift, erreur lors de la génération.

## Modifications générales

-   Pourrait-on fusionner les champs booléens `is_regular` et `is_compensation` dans une `Selection` ?
-   `planning.py`, l.158, pourquoi `> i` et pas `> 1` ?
-   Est-ce que le nom d'un `TaskTemplate` ne pourrait pas être généré automatiquement ?
-   `TaskTemplate` : pourquoi `duration` n'est pas computé ?
-   Pourquoi peut-on créer deux shifts à la même horaire pour le même travailleur ?
-   Changer les `beesddoo` en `beesdoo`
-   Changer `is_regular` en `is_normal` partout ou le laisser comme tel ?
-   Le système de feuille de présence pourrait permettre enlever le champ `is_regular` / `is_compensation` dans les shifts réels, car il est informatif et n'a pas vocation a être rempli pour les volants, ce qui est source d'incompréhension pour le moment.
        _Non, car le calcul des compensations est basé sur ce champ pour les réguliers. Si le shift est normal, rien ne se passe au niveau des compteurs._
-   Pourrait-on enlever la différence terminologique dans le code entre Shift et Task ?
-   Pourquoi est-ce que le champ `worker_id` de Task n'est pas obligatoire ?
-   Donner la possibilité aux utilisateurs de configurer les paramètres du code ?

## Issues
-   Après chargement de la traduction fr_BE, apparition de dates au format `%%Y-%%m-%%d`
-   Lorsque le Generic User se connecte, il arrive directement sur la génération d'une feuille de présence.
-   Dans le .xml, remplacer `context_today()` par `datetime.date.today()` afin de résoudre le bug suivant : `context_today() is not defined in XML`

## Notes diverses

-   Il est impossible de mettre une note et de direction la cocher comme "lue" car le champ `is_annotated` est évalué lors de la sauvegarde.
-   Lorsqu'un travailleur est présent en Added Worker, qu'on l'enlève et qu'on le rajoute, la contrainte d'unicité est marquée comme violée à cause de l'ordre d'execution des commandes SQL.

## Informations, liens

Utiliser les recommandations de l'OCA et Black pour la réécriture du code.

Méthodes des fields.Date et fields.Datetime : `fields.Date.to_string()`, `fields.Date.from_string()`, `fields.Datetime.from_string()`. La méthode `from_string` sur une string Datetime permet d'obtenir une Date.
