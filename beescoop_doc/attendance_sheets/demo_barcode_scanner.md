# Odoo - Tutoriel Barcode Scanner


Standard module `barcodes` gives an abstract class `BarcodeEventsMixin`,
which allows an object inheriting from it to react to barcode events.

## Initialisation

- Add `barcodes` to the module dependencies

- Inherit from `barcodes.barcode_events_mixin`

- Implement method `on_barcode_scanned`

- Add the following field to form view :
```xml
<field name="_barcode_scanned" widget="barcode_handler"/>
```

## Informations

- `on_barcode_scanner` is behaving as if decorated with `@api.onchange`.

  - _Readonly fields are not saved into the database even when they change their value through an onchange._


- If your form view contains text fields, they will be selected by default on edition,
and the scanner's input won't trigger any method but behave as a keyboard input.

 - _For a better user experience, your form view shall not contain any text field._


- If you need to update `One2many` fields with the method, create an object with `new()` and add it to the recordset :

```python
self.record_ids |= self.record_ids.new({
    "field1": value1,
    "field2":"value2"
})
```
