# Attendance sheets - Suivi
Ce document précise la création d’un module permettant de valider les présences aux shifts des travailleurs de la Bees Coop.

## [Réunion du 10/09/19 à CIE]
_Personnes présentes : Rémy & Bart_

#### Situation actuelle
Actuellement, le salarié ou super-coopérateur qui gère le shift possède une feuille de papier générée automatiquement grâce aux données en base de donnée, sur laquelle il vient marquer la présence au shift des collaborateurs. Il doit aussi gérer les compensations.

Le bureau des membres vient ensuite, deux fois par semaine, relever ces papiers et inscrire les présences. Les présences ne sont donc pas actualisées en direct, ce qui pose un souci pour les travailleurs, notamment lorsque leur statut change à cause de ce délai. La méthode est par ailleurs assez longue et pénible.

#### Cahier des charges
La validation doit se faire au début car la fin des shifts est toujours désordonnée, tout en laissant la possibilité d’ajouter les travailleurs en retard.

Les travailleurs réguliers doivent être ajoutés à la liste, mais il faut aussi pouvoir ajouter les coopérateurs volants ainsi que les remplacements. Peut-être qu’on peut générer la fenêtre à partir des shifts, directement ?

Les travailleurs ne doivent pas être en mesure de se rajouter des validations ou de valider les créneaux de leurs amis.

#### Cartes de coopérateurs
On peut imaginer intégrer le scan des cartes, pour faciliter la procédure. Il ne faudrait pas, cependant, que les travailleurs puissent scanner plusieurs fois, ou scanner les cartes de leurs amis : il faut donc un contrôle de la part du salarié.

#### Développement
Il se fera sur la version 9, afin de permettre l’intégration rapide du module. Peut-être est-il judicieux de travailler sur la version 12 puis de backporter ce travail, afin de faciliter la migration future ?

#### Fonctionnalité déjà présente ? Pas vraiment
Une fonctionnalité similaire existe déjà, dans Shift Attendance. Elle n'est pas utilisée par peu simple pour ajouter ou supprimer des travailleurs, et pour visualiser les présences. On pourrait ajouter un wizard qui permettrait l'ajout et la validation des shifts. Si jamais quelqu'un est en retard, on peut réutiliser le wizard, mais les shifts déjà validés ne sont plus visibles. Si on veut annuler une validation, procédure longue à la main.
Si jamais le créneau est lancé 10 minutes avant (toute l'équipe présente), il faut faire attention à ce que le bon créneau soit choisi. Il y a aussi plus d'un créneau par heure et par jour (en fonction de la tâche à effectuer) et il faut être en mesure de choisir ce créneau.

#### [Réunion du 12/09/19 à CIE]
_Personnes présentes : Virginie & Bart_

#### Solution proposée
Cette solution repose sur la création d'une fiche de présence pour chaque shift. Celle-ci est déjà remplie avec le nom des travailleurs réguliers et volants prévus pour le créneau et permet un suivi clair de l'activité et des présences (coloration différente des lignes, status confirmed).

Les travailleurs scannent leurs cartes en arrivant. La fiche de présence pour le shift en question est automatiquement remplie avec leurs noms et statuts (comment être certain que c'est bien pour ce shift qu'ils arrivent ? Avec un intervalle ?). Cela permet une plus grande flexibilité, pas besoin de rester derrière le poste à noter les arrivées.

La fiche doit ensuite être complétée (avec le type du shift et la présence) puis validée par un salarié. La validation nécessite qu'il se log dans le système et entre son mot de passe (par le biais d'une fenêtre pop-up).

Il faut être en mesure de gérer les diverses exceptions : on prévoit un champ libre pour cela. Le bureau des membres sera notifié de ce brouillon (quand ?) et pourra prendre la décision adaptée.

La gestion des retards nécessite encore une solution efficace. Faut-il ajouter un bouton "retardataire", qui éditerait le champ correspondant à l'utilisateur ou le créerait dans la fiche de présence ?

Autre proposition, si l'appel est fait à chaque fois : accès à une feuille de présence éditable mais sauvegardable uniquement avec login. Ce login est ajouté à la feuille de présence comme "validateur".
La feuille de présence reste sur un odoo standard avec une interface limitée. Est-il nécessaire de le laisser tout le temps allumé ? Shift Attendance resterait accessible uniquement au bureau des membres.

#### Questions :

- Est-ce réellement utile d’utiliser un système de scan des cartes ?         
- Est-ce que l’appel est fait à chaque fois ?


#### A faire

- Préciser les points d'ombres (après retour de Bart)
- Specification
- Slide de présentation pour le bureau des membres



## [Réunion du 20/09/19 à la BEES]
_Personnes présentes : Geneviève & Bart_

#### Solution proposée – attendance_sheet

#### Matériel
Un ordinateur dédié au module avec un compte générique et un scanner de carte sont disposés dans l'espace travailleur.

#### Fiches de présence
La solution proposée repose sur un système de fiches de présences, par créneau horaire. La liste des fiches pour la journée est affichée sur l'ordinateur, avec un onglet dédié à l'horaire en cours afin d'y accéder facilement.

#### Arrivée des travailleurs
Les travailleurs peuvent consulter, en arrivant, la liste pré-remplie avec les travailleurs attendus pour le shift en question (réguliers et volants inscrits). En scannant sa carte, le travailleur est ajouté à la liste s'ils ne l'était pas et son statut passe à attended. On peut aussi l'ajouter à la main avec une barre de recherche, et supprimer les travailleurs qui n’étaient pas attendus à la main, en cas d’erreur.

#### Code couleur présence
_Gris_ : présence pas encore traitée  
_Jaune_ : absent  
_Vert_ : attended

#### Validation de la fiche
A la fin du shift, la fiche doit être validée. Elle ne sera alors plus modifiable. Pour valider la fiche, le super-coopérateur ou salarié doit entrer ses identifiants, et son nom sera inscrit sur la fiche. Il doit auparavant avoir complété le statut de chacun des shifts (attended, absent, excused, excused – absolute necessity). Si aucun des super-coopérateurs et salariésn'est présent, la fiche passe en non validée.

#### Fiches non validées
Si l'horaire d'un créneau est passée et la fiche en question non validée, elle est affichée en rouge dans la liste. Un salarié devra les valider à la fin de la journée. Le bureau des membres reçoit un email s'il y a des fiches non validées après la fin de la journée.

#### Fiches annotées
Les fiches possèdent un espace d'écriture libre, qui permet d'inscrire des remarques et ainsi de gérer les exceptions. Cela peut-être utile, par exemple, si un travailleur vient faire son premier shift mais qu'il n'est toujours pas inscrit. Les fiches notifiées sont indiquées par une icone commentaire, et doivent être traitées par le bureau des membres (ils recoivent un email + elle est présente dans la worklist).

#### Envoi d'email aux absents
Lorsque la fiche est validée, cela envoie un email aux travailleurs qui n'étaient pas présents afin de les prévenir et de mettre au clair l'absence.

#### Worklist (accessible uniquement par le bureau des membres)
On y retrouve les fiches non-validées et les fiches notifiées. Icône "notifiée" en rouge si non-traitée dans la worklist, vert si traitée. Chaque fiche, qu’elle soit notifiée ou non-validée, doit être consultée et validée par le bureau des membres afin de disparaître de la worklist.

#### Vue globale administrateur
Les administrateurs shifts ont accès à la vue qui englobe toutes les feuilles et peuvent les modifier même si elles sont validées. Un log permet d'enregistrer toutes les modifications effectuées.

#### Questions :
- Si fiche validée et travailleur en retard ? Impossible normalement car les fiches sont validées à la fin.


## [Réunion du 04/10/19 à la BEES]
_Personnes présentes : Bart_

La prochaine présentation devra être un schéma fonctionnel (voir Prezi).

#### Interface
L’interface doit s’inscrire dans le menu standard. Il faudrait pouvoir mettre en plein écran la liste des travailleurs attendus.

L’onglet « Current shift » n’est pas approprié car il est trop rigide et redondant, il est préferable de laisser la place à l’utilisateur de faire le choix du shift qui l’intéresse. Cela pose un souci, il ne faut pas que l’utilisateur puisse se tromper de feuille de présence et remplir la mauvaise. On pourrait choisir d’afficher un pop-up si l’heure ne correspond pas.De plus, les shifts se superposent d’un quart d’heure, ce qui rend impossible l’automatisation totale du créneau affiché et complique le surlignage du créneau courant.

La sélection des compensations actuelles (Absent, Excused, Excused absolute necessity ) n’est pas claire pour les travailleurs. Penser à une reformulation ou à un texte explicatif.

#### Login
La question du login est problématique : et si le super-coopérateur a perdu le sien ? Certains coopérateurs, notamment les coopérateurs âgés, ne se servent jamais de l’intranet et de leur login. Il faut diminuer au maximum la dépendance à ce système et un lien « login / mot de passe oublié » est donc inapproprié.

#### Types de shifts
Les types de shifts (bureau des membres, magasin, découpe fromage) devraient être affichés et visuellement séparés sur la feuille de présence.

#### Fiches notifiées (annotées), non-validées
L’annotation d’une fiche ne devrait pas empêcher sa validation. Il est plus judicieux de séparer les fiches non-validées et les fiches annotées dans deux worklists. La « double-validation » lorsque la fiche est notifiée n’est pas simple ni claire. On peut imaginer un bouton « Marquer comme lu » près de l’annotation à la place.


## [Réunion du 07/10/19 à CIE]
_Personnes présentes : Rémy_

Il y a un super-coopérateur par shift de travail, l’affichage de celui-ci pour la feuille de présence n’est donc pas judicieux. Il est préferable d’afficher la personne qui a validé la feuille.

#### Wizard de validation
La validation se fait par le même bouton, de deux façons différentes. Si utilisateur courant a les droits d’écriture (salarié), le wizard prend cela en compte, affiche une simple pop-up et évite la validation par carte / barcode.
Si l’utilisateur courant n’a pas les droits (ordinateur loggé toute la journée), on affiche un champ barcode (qui nécessite un scan par carte). La feuille ne peut être validée que si l’utilisateur est super-coopérateur ou salarié.

#### Créneau courant et message d’avertissement
Créneau courant : si heure courante différente de  celle du shift, on affiche un message d’avertissement dans la vue formulaire.

#### Changement du statut des feuilles de présence

Seulement créer deux statuts : validé et non validé, avec un booléen qui indique que l’annotation a été traitée en plus.

#### Types de shift
Vide pour les volants qui s’ajoutent. La liste est triée par type de shift.
On ne touche pas au compteur de compensation, mais on créé / édite des objets shifts.


## [Réunion du 08/10/19 - BEES Coop]
_Personnes présentes : Bart, Geneviève_

#### Interface
- Remplacer le surlignage en vert des fiches de présences validées par un surlignage en gris.
- Bouton Save Draft en haut à gauche, à côté du bouton Validate Sheet.
- Séparer la liste admin des feuilles de présences et la mettre en bas du menu.
- Mettre le type de tâche le plus à gauche dans la liste.
- Numéroter les lignes de la liste des travailleurs.


#### Créneaux horaires
Ceux-ci ne devraient pas être hardcodés mais dynamiquement déterminés en fonction des shifts prévus pour la journée.

#### Types de tâche
Ajouter un menu déroulant permettant de choisir le type de shift. Les options proposées correspondent aux types de shifts initialement prévus pour le créneau, pas à tous les types en base de donnée ni à ceux non remplis.

Les travailleurs seront ajoutés en Shop par défaut. Idéalement, la liste restera triée par type de tâche, avec Shop le plus en bas.

#### Gestion des compensations
Les travailleurs volants ne peuvent pas être excusés, ils sont présents ou absents. Une absence de leur part correspond à une simple compensation (celle-ci est gérée par le système). Seuls les statuts présent et absent devraient être affichés.

Les travailleurs réguliers peuvent être présents, absents, excusés, excusés (nécessité absolue), chacun des statuts correspondant à un nombre de compensation. Actuellement, lors de l’appel, le super-coopérateur coche un nombre de compensation à attribuer et cela lui permet de visualiser ce qu’il attribue. Les compensations sont ensuite traduites en statuts correspondants par le bureau des membres.

Si le super-coopérateur devait désormais choisir le statut et non plus le nombre de compensations, on risque de créer un problème de compréhension. Il faudrait afficher les options suivantes pour les réguliers :

- Présent
- Absent - 0 compensation
- Absent - 1 compensation
- Absent - 2 compensation


#### Ajout des réguliers
Actuellement, lorsqu’un régulier rejoint un créneau qui ne lui était pas assigné en base de donnée, il doit préciser si le shift est de compensation ou non. Ce dernier cas peut arriver en cas d’erreur d’encodage : c’est bien son shift mais il ne lui est pas attribué dans la base.

Formulaire dynamique dans le cadre de la deuxième liste (inline ou non ?), case à cocher pour choisir le type de shift quand c’est régulier. Si il est placé en régulier, on ajoute cette information à l’annotation.

#### Remplacement
Un champ supplémentaire dans la liste de présence avec un menu déroulant permettra d’indiquer si un remplaçant effectue le shift. Il pourra être pré-rempli si l’information était déjà en base de donnée. On ne peut pas être remplacé par un volant.

A l’avenir, il faudra ajouter le champ soutenu par. Un travailleur ne pourra être soutenu et remplacé en même temps, on peut donc imaginer choisir l’option dans le même champ.

#### Champs texte
Le champ d’annotation doit être tout le temps affiché, il faut enlever la case à cocher.

Ajouter un champ permettant un compte-rendu de l’ambiance générale ainsi qu’un emplacement permettant d’exprimer son ressenti sur le nombre de travailleurs (y en avait-il trop ? pas assez ?).

#### Nombre de travailleurs
Afficher le nombre de travailleurs maximum, en faisant la somme des différents types de tâches. Le nombre de travailleurs présent sera computé une fois la fiche validée.

#### Mails
Les mails envoyés au bureau arrivent dans une seule boite email, ce n’est pas une liste de diffusion.

#### Retardataires une fois la liste validée
Ceux-ci, si acceptés, devront demander à un salarié d’éditer la feuille de présence en se connectant à son compte. Cela restera une exception.

#### Créneaux AG
Ces créneaux seront gérés à la main car tout ce qui est dans les feuilles de présence est géré par les Task template id.


## [Réunion du 08/10/19 - CIE]
_Personnes présentes : Rémy_

#### Séparation en deux listes
Faire deux listes : une avec les travailleurs déjà prévus, une avec ceux ajoutés. Cela simplifiera la création des mécanismes propres à chacunes.

Lorsque les personnes non attendues scannent, un message leur demande de s’ajouter à la main.  Cela permet de considérer les questions importantes relatives à leur présence. Exemple : si une personne scanne et qu’elle venait pour remplacer quelqu’un, elle doit le préciser et n'est pas ajoutée automatiquement. Il faut faire attention à l'automatisation.

#### Créneaux horaire
Grouper les tasks template par heures + jour de la semaine. La feuille de présence brouillon se met à jour automatiquement. Si on ajoute un shift en le liant à un task template alors il sera dans la feuille de présence.

#### Champs texte
On perd le booléen indiquant que la fiche est annotée, mais on peut le remplacer par une méthode is_empty(). Il ne faut pas que le champ soit en HTML car il ne sera jamais vide. Attention aux espaces, etc. (trimer le champ).

## [Point technique du 25/10/19]

La question de la création des feuilles de présences est complexe. On ne veut pas hardcoder les créneaux horaires car ceux-ci doivent être dépendants des shifts créés. Plusieurs solutions sont proposées :

#### CRON journalier

Un CRON créé chaque jour les feuilles de présence pour la journée suivante. Tous les shifts pour cette journée sont récupérés puis parcourus par date de début et de fin. Pour chacun de ces créneaux, la feuille est créée si elle n’existe pas et le shift y est ajouté à la liste des expected_shift.

Cela créera des feuilles de présences même si les shifts sont ajoutés au préalable à la main et non par le générateur de planning. Ainsi, pour une AG par exemple, on pourra avoir des feuilles de présence pour un seul shift, comme c’est déjà le cas avec Jasper.

Le problème : les travailleurs volants peuvent s’inscrire à la dernière minute. Cette solution nécessite de bloquer leur inscription à la veille. La création / modification de shifts directement dans le back-end ne sera pas non plus prise en compte dans la feuille de présence.

#### CRON de mise à jour toutes les 5 minutes.

Celui-ci viendrait mettre à jour les feuilles de présence déjà créées, toutes les 5 minutes par exemple. On ne récupérerait cependant que les shifts inclus dans les créneaux horaires déterminés par le planning. Cette solution peut ralentir Odoo.

Création des fiches lors de la génération du planning

On perd alors tout ajout ou modification ultérieur des shifts. Impossible de mettre en place un champ calculé car on ne peut pas indiquer dans le @api.depends un champ déclenchant le trigger qui ne soit pas attribut.

#### Surcharge des méthodes write() et create() des Tasks

Dès qu’un shift est créé, une recherche est effectuée sur le créneau en question avoir de trouver la feuille de présence correspondante. Si elle n’existe pas, elle est créée. Le shift est ajouté à la liste des expected_shift de cette feuille de présence.
La mise à jour d’un shift se répercute sur la feuille de présence qui lui correspond, tant qu’elle n’est pas validée, grâce à une surcharge de la méthode write().

Cette solution a l’avantage d’être efficiente. On se risque cependant à modifier des méthodes clés. Tous les cas d’écriture devront être traités, ce qui est coûteux. Nécessité de mettre en place des tests unitaires.

#### Wizard de création : solution choisie

Un bouton dédié (enlever l’usuel Create) permet de générer une feuille de présence via un wizard. L’utilisateur doit choisir parmi les Fiches disponibles listées, en fonction de l’horaire courant. Les fiches sont disponibles s’il existe au moins un shift inclus dans l’horaire courant et que la fiche n’est pas déjà créée. Si l’horaire courante recouvre plusieurs créneaux et que les fiches n’ont pas encore été créées, elles sont proposées.

Cas problématique : la fiche est créée dans l’intervalle de temps autorisé ( 15 minutes avant le début réel par exemple) et un volant s’inscrit dans cet intervalle.

Avantages : pas besoin de mettre à jour les fiches de présence. On ne crée pas automatiquement des fiches qui n’auraient pas besoin de l’être.

## [Réunion du 10/12/19 - CIE]
_Personnes présentes : Virginie, Geneviève, Bart_

Enlever l'attribut _Cancelled_ du statut des shifts proposé dans la feuille de présence.

Ne pas ajouter un shift annulé lors de la génération de la feuille de présence. Tous les autres statuts doivent se retrouver sur la feuille de présence, si le shift a déjà été pré-rempli.

Le _Maximum number of worker_ doit se trouver en haut et être calculé à partir des shifts créés et non pas des créneaux

L'annotation du wizard de validation doit être pré-remplie avec le message signifiant qu'un travailleur régulier effectue son shift régulier en _Added_.

Si l'utilisateur scanne et qu'il appartient au groupe _CooperativeAdmin_, la validation doit être effective

Enlever le default super_coop

**Simplifier les statuts**

Comment se déroule la validation des feuilles de présence sans système de carte, pour les autres supermarchés ? **Booléen de configuration activant le scan des cartes** ?

Enlever la vue _Shift attendance_

Ajouter le numéro de la semaine sur la vue liste et sur le nom

Créer documentation
