# Attendance Sheet - Barcode scan - Documentation

## Description

Le scan des cartes de coopérateurs doit fluidifier le remplissage de la fiche de présence.

Un scanner (_quel type ?_) est connecté à l'ordinateur sur lequel les feuilles de présence sont affichées.

Il est mis en détection automatique. Lorsqu'un coopérateur scanne et qu'il remplit les conditions suivantes, il est passé en _Attended_.

S'il n'était pas attendu et qu'il scanne, cela l'ajoute ? A statuer.
        Il pourrait alors se tromper s'il fait un échange, mais on gagne en rapidité.
        Il ne faudrait pas non plus pré-remplir le `Task Mode`, pour éviter les erreurs.

Le wizard de validation fait aussi appel au scan.

## Analyse technique

Les codes-barres existent déjà et sont générés pour chaque coopérateur puis affichés sur sa carte.

Le module standard `barcodes` possède une classe abstraite `BarcodeEventsMixin` qui pourrait être adéquate :

>Mixin class for objects reacting when a barcode is scanned in their form views
which contains `<field name="_barcode_scanned" widget="barcode_handler"/>`.
Models using this mixin must implement the method on_barcode_scanned. It works
like an onchange and receives the scanned barcode in parameter.

Si un utilisateur scanne mais qu'il n'a pas les droits ou qu'il n'est pas supposé pouvoir travailler, que faut-il faire ?
Dans quel cas casser le flux en affichant un avertissement ? (en fonction de son statut)

## Problèmes

>**Readonly fields are not saved into the database even when they change their value through an onchange.**  

-   `on_barcode_scanned` est une méthode ONCHANGE, elle ne sauve donc pas les données ! Problème lors de l'appel d'autres méthodes.
-   La méthode `on_barcode_scanned` n'est pas appelée lorsque le curseur est sur un champ texte, ce qui est automatique lors de l'édition.
        Proposition : créer un wizard ?
-   Voir les fichiers javascript module `barcodes`pour les évènements.  

>However, for security reasons, a keypress event programmatically crafted doesn't trigger native browser behaviors. For this reason, BarcodeEvents doesn't intercept keypresses whose target is an editable element (eg. input) or when ctrl/cmd/alt is pushed. To catch keypresses targetting an editable element, it must have the attribute barcode_events="true".
-   Chercher du côté de l'attribut ` default_focus` pour voir ou on peut l'ajouter


Voir [documentation](https://github.com/odoo/odoo/blob/cd48a806ac636806130efcf5c055f73758cb0902/addons/barcodes/doc/index.rst#barcode-events)
