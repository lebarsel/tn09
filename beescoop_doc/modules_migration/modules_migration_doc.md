# Modules migration

Utilisation de l'utilitaire `2to3` pour la migration python.

## partner_firstname

### 10.0

Peut-être mettre la clé *installable* à *True* dans `__manifest__.py`

**Ne fonctionne pas en version 12. Faut-il prendre le module de l'OCA ?**

## beesdoo_base

- Nombreux conflits lors du `git format-patch --keep-subject`
  Pour tester l'intégrité de la version ainsi obtenue, on retente l'installation du module.
  Celle-ci s'avère impossible.

  Finalement, le module est simplement copié et l'historique des commits perdu.

Problème : le module **report** est désormais séparé entre **base** et **web**, l'objet correspondant n'existe plus. Il semblerait que le controleur en question était "back-porté", la route est définie de base dans Odoo 12 : on peut supprimer le code.

Problème dans `wizard/views/partner.xml`  


```
Error while validating constraint  
Element <field name="portal_id"> cannot be located in parent view
Error context : View Unfuck Portal Wizard
```

Suppression de la vue `beesdoo_portal_wizard_view` (l'attribut concerné n'est plus présent dans le module **portal** version 12.)

## beesdoo_shift

Changement de la vue cron.xml.

Problème vue kanban : `Invalid view Task Template Kanban definition in beesdoo_shift/views/task_template.xml`

**Problème** Formulaire de création d'une task action qui a changé. Voir bug précédent.


**Problème** : recurring worker ne s'ajoute pas au planning action lorsqu'on le créé

**Problème** : `TypeError: '>=' not supported between instances of 'datetime.date' and 'bool'`

`File "/home/elouan/beescoop_shifts/beescoop_shifts_12_2/coopiteasy/obeesdoo/beesdoo_shift/models/cooperative_status.py", line 220, in _set_regular_status`

Ceci car les dates ne sont plus des string, et `temporary_exempt_start_date` n'est pas set donc possède la valeur *False*

**Mise à jour du système de dates**

> Toutes les dates, enregistrées dans la base de données et stockées dans lesobjets, sont enregistrées comme étant des dates UTC (temps universel co-ordonné). L’UTC ne connait pas le changement d’heure été/hiver. Il permet d’être une référence commune pour toutes les heures du globe. C’est lors de l’affichage d’une date que celle-ci est transformée en heure locale en utilisant le fuseau horaire défini dans les paramètres d’Odoo ou de l’utilisateur. Afin de construire les shifts fictifs, la première réalisation ajoutait simplement les 28 jours en plus à la date à l’heure UTC du shift de départ. Ce qui impliquait un décalage des heures de début et de fin de shift aux passages à l’heure d’étéet à l’heure d’hiver. En effet, à l’heure d’hiver belge, unshiftqui commence à12h, heure légale belge, se passe à en réalité à 11h, heure UTC. Le même shift réalisé, à l’heure d’été belge, qui commence toujours à 12h, heure légale belge,se passe à 10h, heure UTC. Cela est dû au fait qu’en hiver l’heure légale belge est UTC+1 et en été l’heure légale belge est UTC+2. Il faut donc que l’ajout des jours tienne compte de ce changement d’heure afin de le répercuter à l’heure UTC stockée dans le shift. L’ajout des jours se fait via la méthode `timedelta()`du module `datetime` de Python. Pour comprendre le problème, il faut comprendre comment Python gère les dates et les heures. Il y a deux types de date différentes : les dates brutes qui ne sont liées à aucun fuseau horaire appelées naïves et les dates qui sont liées à un fuseau horaire appelées non-naïves. Lorsqu’on ajoute un nombre de jours à une date non-naïve, le fuseau horaire est laissé intacte. Une date,avant le changement d’heure, qui est liée au fuseau horaire UTC+1, après l’ajout d’un nombre de jour qui la fait passer à une date après le changementd’heure, conserve le fuseau horaire UTC+1 là où elle devrait se voir attribué le fuseau horaire UTC+2. La solution est donc de corriger le fuseau horaire une fois la date modifiée afin que la conversion vers l’heure UTC répercute bien le changement d’heure


**Problème** Dans le wizard *Suscribe* (abonnement aux shifts). Le champ info_session_date est un *Date*
alors que c'est un *Datetime* dans le modèle.

**Problème** Lorsque l'on veut changer l'état d'un travailleur et que cela doit changer l'état de son shift, l'exception `You cannot change to the status Attended if there is no worker defined on the shift` est levée.

**Problème** Lors du changement de travailleur pour un shift, `TypeError: 2019-09-19 14:46:23 (field cooperative.status.irregular_absence_date) must be string or date, not datetime.`

**Problème** Suscribed workers ne sont pas ajoutés aux shifts !

### BEES Coding Sprint

A faire :
- Mettre admin dans le groupe `cooperative_admin`

Divers :
Pourquoi un irregular peut s'inscrire à des TaskTemplate ?


## beesdoo_website_shift


Odoo retourne désormais des objets python natifs pour les dates et datetimes.

**Bug** Erreur d'affichage pour la route `/shift_template_regular_worker`

Problème dans `my_shift_website_templates.xml`

Erreur : `AssertionError: Element odoo has extra content: template, line 9`  

Element de réponse :
>`page="True"` is indeed not the correct way to define pages in 11.0 anymore and will prevent loading `<template/>` data correctly.

>In 11.0, website pages have their own model (`website.page`) and are not simple ir.ui.view records anymore. To define a page, there are two ways:

```xml
<record id="my_page" model="website.page">
    <field name="name">My Page</field>
    <field name="website_published">True</field>
    <field name="url">/my_page</field>
    <field name="type">qweb</field>
    <field name="key">my_app.my_page</field>
    <field name="arch" type="xml">
        <t t-name="my_app.my_page_template">
             <t t-call="website.layout">
                <!-- ... -->
             </t>
        </t>
    </field>
</record>
```

or

```xml
<template id="my_page_template">
    <t t-call="website.layout">
        <!-- ... -->
    </t>
</template>
<record id="my_page" model="website.page">
    <field name="name">My Page</field>
    <field name="website_published">True</field>
    <field name="url">/my_page</field>
    <field name="view_id" ref="my_page_template"/>
</record>
```

>Both ways are fine. The first one has the advantage to define the page "with one record data" and the second has the advantage of using the `<template/>` shortcut and allows to inherit from this template.

>This new model allows to integrate the pages in the page management system which, as you can see, allows some new page features. To name a few: choosing the exact URL you want for your pages (without /page/) or choosing if the page is published or not.

Solution retenue : suppression de l'attribut `page=True`

**Problème** dans `my_shift_website_templates.xml` : impossible d'ajouter le lien vers *My Shifts* car le menu n'est pas complet.

Problème réglé, voir code.

**Problème** : `AttributeError: 'NoneType' object has no attribute 'split'`

The error occured while rendering the template `portal.portal_layout` and evaluating the following expression: `<div t-field="user_id.partner_id" t-options="{&quot;widget&quot;: &quot;contact&quot;, &quot;fields&quot;: [&quot;email&quot;, &quot;phone&quot;, &quot;address&quot;, &quot;name&quot;]}"/> `

Solution : installer `l10n-belgium` afin de créer les comptes analytiques, pour pouvoir créer des worker.

**Problème** : le nom n'est pas affiché, sûrement un problème de `partner_firstname`

**Problème** : Boutton *suscribe* ne fonctionne pas pour les travailleurs volants.

## Questions
Comment sont liés les *users* et *worker* normalement ?
--> Lors de l'achat de parts

Pourquoi un *ResPartner* est lié à plusieurs *CooperativeStatus* ?
Pourquoi n'ai-je pas le wizard *Créer demande de souscription* ?

**Checker les Access Rules pour les créneaux de shifts public**.

## A faire
- Debugger les vues : ok
- Ajouter *My Shifts* au menu : ok
- Régler les warnings
- Faire les tests
- Corriger affichage vues

## Attention

Plus de highlight des lignes ?
`unsubscribe_from_today` : mauvais nom de méthode ?
CRON jobs
changer `bg-warning` class
permettre configuration des horaires, contacts... pour faciliter l'intégration future de nouveaux clients
Problème d'affichage viendrait du comportement en bloc ?
