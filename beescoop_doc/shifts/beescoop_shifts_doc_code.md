
### Analyse du code : beesdoo_shift

#### beesdo_shift
##### Classes

`cooperative_status.py`

- **ExemptReason**
- **HistoryStatus** : On garde une trace des status précédents, si modifications il y a.
- **CooperativeStatus** : Le status d'un individu au sein de la coopérative.
- **ShiftCronJournal** : Log journalier du travail de comptage de shifts
- **ResPartner** (hérite de `res.partner`)

`planning.py`

- **TaskType** : Le type de tache à effectuer
- **DayNumber**
- **Planning** : Possède des méthodes permettant de récupérer le planning suivant ou de le générer.
- **TaskTemplate** : Le modèle d'un créneau. Possède une méthode `_generate_task_day` permettant de générer les shifts.

`task.py`

- **TaskStage**
- **Task** (hérite de `mail.thread`) : système de messages (?)

##### Counter Update Journal

Compte du nombre de shifts réalisés. Celui-ci est effectué par *cron*.

### Analyse du code : beesdoo_website_shift

#### Routes

__*/my/shift*__
> Personal page for managing your shift

__*/shift/< int:shift_id\>/suscribe*__
> Suscribe the current connected user into the given shift, regarding certains conditions.

__*/shift_irregular_worker*__
> Show a public access page that show all the available shifts for irregular worker.

__*/shift_template_regular_worker*__
> Show a public access page that show all the available shift templates for regular worker.

#### Vues

Il existe fichiers XML différents qui définissent des vues :

- `my_shift_website_templates.xml` :
- `shift_website_templates.xml` : définit les vues publiques
- `res_config_views.xml`

#### Points importants pour l'échange de shifts

- Les shifts ne sont créés que toutes les 4 semaines, comment faire pour échanger un shift qui aura lieu plus tard ?

#### Déroulement

Installation de deux versions d'Odoo (avec environnement virtuel), 9 et 12

Nom des bases de données : **beescoop_shifts_db_[9|12]**

##### Odoo 9

Pour lancer le serveur web
`python odoo_9/odoo.py -d beescoop_shifts_db_9 --db-filter=^beescoop_shifts_db_9$ --conf=odoo.conf`

**Il y a peu y avoir des requirements.txt dans d'autres modules qu'Odoo ! Utiliser `find` pour les trouver**

- [Installation de pipx](https://pipxproject.github.io/pipx/)
- [Installation de git aggregator](https://github.com/acsone/git-aggregator)  
`gitaggregate -c repos.yaml`

**Clonage du repo git - Clé SSH**

Pour lister les clés SSH : `ls -al ~/.ssh`

Génerer clés SSH pour github : `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"`

Ajouter la clé à l'agent SSH
```
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa
```

**Création du fichier de configuration**

Utiliser un utilitaire bash `repo2conf` qui prend le fichier .yaml de gitaggregator et donne les lignes nécessaires.

**Odoo 9 - Différences de fonctionnement**

Developer mode dans *About Odoo*

**Installation d'Odoo 9 - Troubleshooting**

- Problème lors du lancement d'Odoo   `ImportError: /home/elouan/beescoop_shifts/beescoop_shifts_9/venv/local/lib/python2.7/site-packages/psycopg2/.libs/libresolv-2-c4c53def.5.so: symbol __res_maybe_init version GLIBC_PRIVATE not defined in file libc.so.6 with link time reference`

Solution : problème avec la version 2.1 de pyscopg. Il faut mettre à jour la librairie :

`pip install pyscopg2 --upgrade`

EDIT : désintaller et réinstaller de cette façon :

`pip install --no-binary :all: psycopg2`

Pour voir le liste des modules installés et leurs versions :  
`pip list`

- Les modules n'apparaissent pas.

Il faut mettre à jour la liste des modules, avec l'interface web : *Update Apps List*

##### D'Odoo 9 à Odoo 12 #

**Modules à passer en 12** (car requis pour les modules shifts)

git am --continue

- partner_firstname
- beesdoo_base
- beesdoo_website_shift
- beesdoo_shift

peut-être qu'il manque une dépendance non ajoutée dans les requirements

faire une base de donnée avec tout le code en 9
: si cela fonctionne c'est qu'il faut installer un module comme indiqué précedemment

##### Procédure de migration

- Avoir une version 9 des modules clonée du repo.
`git clone https://github.com/[...]/$REPO -b 9.0`

- Créer une branche 12.0 vide
- Créer une branche 12.0-mig-$MODULE à partir de celle-ci

- La commande suivante effectue un différentiel et ne garde que les commits ayant attrait au module.

`git format-patch --keep-subject --stdout origin/12.0..origin/9.0 -- $MODULE | git am -3 --keep`

Adapter le module à la version 12 et commit les modifications.

------
