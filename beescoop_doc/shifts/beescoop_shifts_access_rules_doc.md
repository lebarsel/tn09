# Beescoop Shifts - Access Rules
Ce document présente les différents groupes du module `Beesdoo Shift`.

Tout le monde peut lire le **Cooperative Status** , le **Cooperative Status History** et les **ExemptReason**.



### Attendance Sheet Access

_Permet d'éditer des feuilles de présence et les valider via un scan de carte._

#### Permissions  
- Editer les **Attendance Sheet**

### Shift Attendance  

_Permet de gérer les présences aux shifts._

##### Permissions

- Lire les **Shift Stage**, **Shift Types**,**Shift Template**,**Planning** et **DayNumber**
- Editer les **Shift**

### Shift Management  

_Permet de gérer les shifts (en créer, supprimer)._

**Groupes inclus** : Shift Attendance

##### Permissions

- Gérer les **Shift**

### Planning Management  

_Permet de gérer le planning des shifts ainsi que leur configuration._

**Groupes inclus** : Shift Management  

##### Permissions

- Gérer les **Shift Type**, **Shift Template**, **Shift Stage**, le **Planning** et les **DayNumber**

### Cooperative Admin  

_Permet la validation des feuilles de présence sans scan de carte, leur configuration ainsi que la gestion du statut des coopérateurs._

**Groupes inclus** : Attendance Sheet Access, Planning Management

##### Permissions

- Gérer les **Cooperative Status**, les **ExemptReason** et les **AttendanceSheet**
- Accéder au **Journal de mise à jour des compteurs** et relancer la tâche
