# Bees Coop - Gestion des shifts - Documentation

### Liens utiles

[Repo git](https://github.com/beescoop/)  
[Site web](http://bees-coop.be)  
[Site web test](gestion-test.bees-coop.be)  
[Wiki OCA pour migrations](https://github.com/OCA/maintainer-tools/wiki)

### Aspect technique du stage

> A l'heure actuelle, cette gestion des travailleurs prend du temps administratif (réencodage de données papier). Un nouveau module devra être créé afin de permettre l'encodage en temps réel des présences et absences des travailleurs via un système de scan des cartes de membre des travailleurs. Le supercoopérateur (travailleur qui encadre les autres travailleurs) devra valider les présences/absences de manière à impacter les compteurs en conséquence.

> En plus, la BEES coop rencontre des difficultés dans la gestion des échanges de shifts entre travailleurs réguliers. Aujourd'hui, les travailleurs réguliers qui ne peuvent être présent à leur shift régulier peuvent écrire sur un papier présenté à l'accueil du magasin une demande d'échange.
Ce tableau et ces papiers ne sont pas efficaces car il faut être physiquement dans le magasin pour y accéder.
Un nouveau module devra être créé pour permettre aux travailleurs de se connecter en ligne et de faire des demandes d'échanges. Tout le modèle de données lié à ces demandes d'échanges et leurs divers statuts devront être créés dans le cadre du stage.

> Enfin, le reporting de l'activité du supermarché est fait à l'heure actuel dans un outil séparé, qui consulte la base de données Odoo. Un autre aspect du stage sera de faire un état des lieux des rapports existants à l'heure actuelle dans cet outil séparé, de faire le bilan de leur utilité et si possible (selon l'ampleur de la tâche) d'intégrer les rapports pertinents dans le système de gestion Odoo, dans l'optique d'obtenir un outil autonome.

### Vocabulaire

**sr** : shift regulier
**sc** : shift de compensation
**res** : ressource

### Notes diverses

Dans Odoo la relation one2one n'existe pas.
On surcharge la méthode `write` de `Model` afin d'enregistrer les champs en base de donnée.
Un travailleur volant ne peut pas se désinscrire d'un shift en ligne.

### Travailleurs réguliers

2h45 toutes les 4 semaines dans un créneau fixe. L’année est découpée en semaine A, B, C, D et les journées, en horaires de 2h45.

Les travailleurs réguliers doivent inscrire à l’une des semaines dans un créneau fixe.

[...]

### Documentation

#### Remplacements La Fève
[Application web](http://remplacements.la-feve.ch/)

Le supermarché participatif **La fève** basé à Genèves a mis en place un outil d'échange de shifts pour ses coopérateurs.

L'outil permet d'ouvrir une demande, avec un formulaire permettant d'inscrire son nom, la date ainsi que l'horaire du shift.

Les demandes sont alors affichées sous forme de liste, et il est possible d'y répondre en apposant son nom. On peut aussi supprimer une demande de la liste.

Cela pose un problème d'intégrité des données : quelqu'un peut faire une demande sans que celle-ci concerne son shift, par mégarde par exemple, voire même supprimer une demande.

### Notes

Problème principal : les shifts des travailleurs réguliers ne sont créés que toutes les 4 semaines.

Le travailleur ne devrait pouvoir proposer qu'une demande pour son shift.

Le travailleur précise quels créneaux l'intéressent. Les travailleurs désirant échanger ont accès à la liste des shifts qu'il peut potentiellement échanger. L'inscription se fait de façon automatique, peut-être avec une validation de la part du super-coopérateur. L'échange se fait aussi sur l'affichage des shifts à venir. Question : combien de créneaux réguliers par semaine ? Comment gérer la temporalité, faut-il donner un intervalle de dates ?
--> Choisir un shift ou un créneau ?

Sinon, propositions dans l'autre sens. Le travailleur désirant échanger "ouvre" son créneau et il recoit des propositions d'échange, qu'il peut accepter.

### Divers

Faire attention aux status !

Le système contient au plus quatre semaines de shifts dans le futur. L'affichage des 13 prochains shifts pour les travailleurs réguliers est donc géré par *shifts fictifs* (ils ne sont pas enregistrés en base de donnée, simplement calculés pour l'affichage)

### Questions
- Comment est géré actuellement l'échange de shifts ?
