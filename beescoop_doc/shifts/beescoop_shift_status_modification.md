# [Analyse] Changement du statut des shifts et des compteurs

[Lien vers le ROI de la BEES coop](https://docs.google.com/document/d/1CGvP1rrrvAlEnIjX6aNPo37rTZkRYq3lgO8hun4XiRU/edit#)

### Problématique

Le mécanisme actuel de présences / absences aux shifts de la BEES est complexe.
On pourrait cependant le simplifier suite au changement de ROI de la BEES coop, et à la simplification de leur système de gestion des shifts.

# Système actuel

Les shifts sont différenciés entre shifts pour travailleurs réguliers et travailleurs volants.
Le compteur associé, qui détermine le statut du travailleur et le nombre de shifts qu'il doit effectué,
est séparé en deux compteurs : un compteur **régulier** et un compteur de **compensation**.

Les volants n'utilisent que le compteur **régulier** tandis que les réguliers utilisent les deux.

_Cette différenciation provient du fait que le compteur régulier pour les réguliers n'est pas décrémenté, mais qu'ils peuvent effectuer des shifts en avance, et que le compteur correspondant doit tout de même être incrémenté / décrémenté. Elle permet aussi de calculer le statut des travailleurs de façon plus précise._

Un shift peut être noté comme **shift de compensation** ou **shift régulier** pour un travailleur régulier.

## Statut d'un shift et opérations correspondantes sur les compteurs

[Voir tableur](https://docs.google.com/spreadsheets/d/1wWHJJ71N3tlPRvJWUYnX_9EE7GpBUF5qRZ3gGfhPWQY/edit#gid=0)

### Code

```python
if self.worker_id.working_mode == 'regular':
    if not self.replaced_id: #No replacement case
        status = self.worker_id.cooperative_status_ids[0]
    else:
        status = self.replaced_id.cooperative_status_ids[0]

    if new_stage == DONE and not self.is_regular:
        if status.sr < 0:
            data['sr'] = 1
        elif status.sc < 0:
            data['sc'] = 1
        else:
            data['sr'] = 1

    if new_stage == ABSENT and not self.replaced_id:
        data['sr'] = - 1
        if status.sr <= 0:
            data['sc'] = -1
    if new_stage == ABSENT and self.replaced_id:
        data['sr'] = -1

    if new_stage == EXCUSED:
        data['sr'] = -1

elif self.worker_id.working_mode == 'irregular':
    status = self.worker_id.cooperative_status_ids[0]
    if new_stage == DONE or new_stage == NECESSITY:
        data['sr'] = 1
        data['irregular_absence_date'] = False
        data['irregular_absence_counter'] = 1 if status.irregular_absence_counter < 0 else 0
    if new_stage == ABSENT or new_stage == EXCUSED:
        data['sr'] = -1
        data['irregular_absence_date'] = self.start_time[:10]
        data['irregular_absence_counter'] = -1

```

## Actualisation des compteurs

```python
@api.model
def _cron_compute_counter_irregular(self, today=False):
    today = today or fields.Date.today()
    journal = self.env['beesdoo.shift.journal'].search([('date', '=', today)])
    if not journal:
        journal = self.env['beesdoo.shift.journal'].create({'date': today})
    # [...] domain = ...
    irregular = self.search(domain)
    today_date = fields.Date.from_string(today)
    for status in irregular:
        if status.status == 'exempted':
            continue
        delta = (today_date - fields.Date.from_string(status.irregular_start_date)).days
        if delta and delta % PERIOD == 0 and status not in journal.line_ids:
            if status.sr > 0:
                status.sr -= 1
            elif status.alert_start_time:
                status.sr -= 1
            else:
                status.sr -= 2
            journal.line_ids |= status
```

# Modifications du ROI

_Extraits du ROI ( 08/09/2019) - féminin et masculin mélangés de même façon que dans le texte intégral_

Point essentiel : plus de shifts bonus pour les réguliers

## Régime volant

Les coopérateurices volantes ont un compteur de shifts afin de calculer leurs 13 shifts par an. Le compteur de shifts commence à 0 et fait -1 toutes les quatre semaines à partir de l’inscription en régime volant. À chaque shift effectué, le compteur augmente de 1. Le principe des shifts “volants” est d’effectuer ses shifts avant que ceux-ci soient décomptés afin de toujours être à jour (et d’avoir son compteur plus grand ou égal à 0 en permanence).

Si la membre n’a pas anticipé (c’est-à-dire si son compteur est à 0 le jour de sa date d’alerte), son compteur passe directement à -2 (un shift de pénalité) à la première échéance (la “date d’alerte”), puis continue de descendre d’un point toutes les quatre semaines. Si la membre remonte à zéro, et retombe en négatif, elle n’a pas de nouvelle pénalité. Si elle remonte à +1 ou davantage, et qu’elle redescend, elle aura une nouvelle pénalité.

### Absence à un shift auquel il s’est inscrit_

Aucun remplacement ne peut être effectué par ou pour un volant.

Le membre “volant” peut annuler ses shifts exclusivement via le Bureau des membres (sur place ou par e-mail) mais jamais via le supercoopérateur du shift auquel il s’est inscrit. Il peut annuler jusqu’à 24 heures avant le shift sans devoir de shift de rattrapage. Passé ce délai, il sera considéré comme absent et aura un shift de rattrapage à effectuer.

Pour une absence non prévue ou si elles annulent leur shift moins de 24h à l’avance, les coopératrices volantes sont sanctionnées par un shift de pénalité (leur compteur fait “-1”).

Exemple 1 : Je suis volante et j’ai déjà fait mon shift pour la période courante (compteur : 1). Je m’inscris à un deuxième shift pour prendre de l’avance, mais je suis absente et ne préviens pas de mon absence (compteur : 0). Je dois donc effectuer un autre shift avant ma date d’alerte.

### Retard dans les shifts_

Les coopératrices en régime volant doivent faire un shift avant leur date d’alerte. Si elles ne le font pas, c’est-à-dire si leur compteur descend en-dessous de 0 parce qu’elles n’ont pas assez anticipé, le système de sanction suit la même logique que pour les coopératrices régulières, mais est adapté à la flexibilité du régime volant. Les compensations sont simplement des shifts supplémentaires, auxquels la travailleuse doit également s’inscrire.

Le principe de la “double compensation” du régime volant s’applique lorsque la travailleuse n’a pas anticipé de shift (c’est-à-dire lorsque son compteur est à 0 le jour de sa date d’alerte). La membre doit alors endéans les 4 semaines :

- Rattraper son shift manqué
- Réaliser un shift supplémentaire
- Réaliser le shift de la période courante

## Régime régulier

Sanction en cas d’absence à un shift ou de retard dans les shifts : _mécanisme de la double compensation_  

Si la membre rate son shift, elle entre dans le mécanisme de la double compensation : pour compenser une absence non prévue, les coopératrices doivent effectuer deux shifts de compensation avant le prochain shift de travail prévu.

Lorsque la membre rate son shift, elle doit donc, dans les 4 semaines suivantes (c’est-à-dire avant son shift régulier suivant) :

- Rattraper son shift manqué
- Réaliser un shift de compensation.

Elle ne doit en outre pas oublier de réaliser son shift régulier suivant.

### Exceptions à la double compensation - La simple compensation

La règle est donc : deux shifts dûs pour un shift manqué. Toutefois et en fonction de la raison de l’absence, la supercoopératrice peut décider que la membre qui a manqué son shift doit uniquement effectuer une simple compensation. Dans ce cas, la coopératrice devra faire un seul shift de compensation avant son shift régulier suivant.

### Réaliser un shift de compensation

Pour réaliser un shift de compensation, la coopératrice doit se présenter 5 minutes avant le début de n’importe quel shift et se présenter à la supercoopératrice de ce shift. Il n’est pas nécessaire de s’inscrire à l’avance pour un shift de compensation. Toutefois si la coopératrice sait à l’avance à quel shift elle compte se présenter pour réaliser sa compensation, elle peut en avertir le bureau des membres, pour faciliter l’organisation du magasin et éviter qu’il y ait trop de coopératrices présentes à un shift.

Si la coopératrice ne s’est pas inscrite à l’avance pour son shift de compensation, la salariée présente au shift peut demander à la coopératrice de venir faire son shift de compensation à un autre moment.

# Nouvelle proposition


Regrouper les deux compteurs en un seul et simplifier les calculs pour les réguliers,
ainsi que le nom des statuts de shift pour qu'ils reflètent le nombre de compensation.

- **Remplacement** : un travailleur régulier qui devait remplacer un autre mais ne vient pas
subit les conséquences de cette absence. Le remplacé n'a pas de pénalités car son compteur n'est pas diminué.

L'absence pour un remplaçant d’un membre aura désormais le même impact sur les compteurs que l'absence normale.

[Voir tableur](https://docs.google.com/spreadsheets/d/1wWHJJ71N3tlPRvJWUYnX_9EE7GpBUF5qRZ3gGfhPWQY/edit#gid=0)

Cela permettrait d'enlever les champs _is_regular_ et _is_compensation_, ou de le remplacer par un seul booléen _is_compensation_ qui ne serait alors qu'informatif.

**Question** : comment changer les statuts des shifts de la meilleure façon ?
- Stage : _Confirmed_, _Attended_, _Absent - 0 CS_, _Absent - 1 CS_, _Absent - 2 CS_, _Cancelled_
- Enlever Absent - 1 CS pour les volants

# Changement du calcul du statut des coopérateurs en fonction du compteur

## Avant

```python
def _set_irregular_status(self, grace_delay, alert_delay):
    counter_unsubscribe = int(self.env['ir.config_parameter'].get_param('irregular_counter_to_unsubscribe', -3))
    self.ensure_one()
    ok = self.sr >= 0
    grace_delay = grace_delay + self.time_extension
    if self.sr <= counter_unsubscribe or self.unsubscribed:
        self.status = 'unsubscribed'
        self.can_shop = False
    elif self.today >= self.temporary_exempt_start_date and self.today <= self.temporary_exempt_end_date:
        self.status = 'exempted'
        self.can_shop = True
        #Transition to alert sr < 0 or stay in alert sr < 0 or sc < 0 and thus alert time is defined
    elif not ok and self.alert_start_time and self.extension_start_time and self.today <= add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'extension'
        self.can_shop = True
    elif not ok and self.alert_start_time and self.extension_start_time and self.today > add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'suspended'
        self.can_shop = False
    elif not ok and self.alert_start_time and self.today > add_days_delta(self.alert_start_time, alert_delay):
        self.status = 'suspended'
        self.can_shop = False
    elif (self.sr < 0) or (not ok and self.alert_start_time):
        self.status = 'alert'
        self.can_shop = True

def _set_regular_status(self, grace_delay, alert_delay):
    self.ensure_one()
    counter_unsubscribe = int(self.env['ir.config_parameter'].get_param('regular_counter_to_unsubscribe', -4))
    ok = self.sr >= 0 and self.sc >= 0
    grace_delay = grace_delay + self.time_extension

    if (self.sr + self.sc) <= counter_unsubscribe or self.unsubscribed:
        self.status = 'unsubscribed'
        self.can_shop = False
    elif self.today >= self.temporary_exempt_start_date and self.today <= self.temporary_exempt_end_date:
        self.status = 'exempted'
        self.can_shop = True

    #Transition to alert sr < 0 or stay in alert sr < 0 or sc < 0 and thus alert time is defined
    elif not ok and self.alert_start_time and self.extension_start_time and self.today <= add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'extension'
        self.can_shop = True
    elif not ok and self.alert_start_time and self.extension_start_time and self.today > add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'suspended'
        self.can_shop = False
    elif not ok and self.alert_start_time and self.today > add_days_delta(self.alert_start_time, alert_delay):
        self.status = 'suspended'
        self.can_shop = False
    elif (self.sr < 0) or (not ok and self.alert_start_time):
        self.status = 'alert'
        self.can_shop = True

    #Check for holidays; Can be in holidays even in alert or other mode ?
    elif self.today >= self.holiday_start_time and self.today <= self.holiday_end_time:
        self.status = 'holiday'
        self.can_shop = False
    elif ok or (not self.alert_start_time and self.sr >= 0):
        self.status = 'ok'
        self.can_shop = True


```

## Après

```python
def _set_regular_status(self, grace_delay, alert_delay):
    self.ensure_one()
    counter_unsubscribe = int(self.env['ir.config_parameter'].get_param('regular_counter_to_unsubscribe', -4))
    ok = self.shift_counter >= 0
    grace_delay = grace_delay + self.time_extension

    if self.shift_counter <= counter_unsubscribe or self.unsubscribed:
        self.status = 'unsubscribed'
        self.can_shop = False
    elif self.today >= self.temporary_exempt_start_date and self.today <= self.temporary_exempt_end_date:
        self.status = 'exempted'
        self.can_shop = True

    elif not ok and self.alert_start_time and self.extension_start_time and self.today <= add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'extension'
        self.can_shop = True
    elif not ok and self.alert_start_time and self.extension_start_time and self.today > add_days_delta(self.extension_start_time, grace_delay):
        self.status = 'suspended'
        self.can_shop = False
    elif not ok and self.alert_start_time and self.today > add_days_delta(self.alert_start_time, alert_delay):
        self.status = 'suspended'
        self.can_shop = False
    # WARNING: check next condition, it's strange
    elif (self.shift_counter < 0) or (not ok and self.alert_start_time):
        self.status = 'alert'
        self.can_shop = True

    #Check for holidays; Can be in holidays even in alert or other mode ?
    elif self.today >= self.holiday_start_time and self.today <= self.holiday_end_time:
        self.status = 'holiday'
        self.can_shop = False
    elif ok or (not self.alert_start_time and self.shift_counter >= 0):
        self.status = 'ok'
        self.can_shop = True
```

# Remarques générales

- Nécessite la création de scripts de migration, pré et post-hook
- Mettre à jour `beesdoo_website_shift` en fonction, si besoin

# Mise en oeuvre

## Changement du code : voir commits

## Script de mise à jour
Changer num de version
Pour tester, sauvegarder la base de donnée et réappliquer la mig. dessus à chaque fois

**Revert info** ?

-> faire correspondre les shifts ayant un `stage` avec un id "absent", "excused", "excused_necessity"
    nouvelles colonnes bool "absent", "excused", "excused_necessity"
    remplir avec True si jamais nom du stage correspond

    post hook :
    trouver id de "absent_0", "absent_1", "absent_2"
    update table, si true alors on met à jour en conséquence
    supprimer colonnes temp

-> faire correspondre le compteur `sr` des `irregular` pour mettre sa valeur dans `shift_counter`
-> `shift_counter` des réguliers doit être la somme des deux compteurs `sr` et `sc`

-> Mettre à jour **revert_info** ou non ?
    -> si compteur mis à jour, changer le revert_info avec une string indiquant que la MAJ n'est plus possible. Voir si une exception est levée.
    -> ne plus changer le statut des shifts après la migration. Changer les compteurs directement, si cela doit être fait !

-> Suppression de `stage_id` afin de mettre un champ Selection `state`, adapté à la `statusbar` (on peut mettre un filtre dynamique dessus)

Couleur désormais reliée à un `TaskType`

--> Solution la plus simple : laisser toutes les possibilités. Aligner le code. Aligner la doc (effet sur les compteurs).
